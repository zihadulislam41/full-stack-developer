﻿$(document).ready(function () {
    $('.left-nave-toggle-btn').on('click', function () {
        if ($(this).hasClass('open-menu')) closeNav();
        else openNav();
    });
});
function openNav() {
    $('.left-nave-toggle-btn').addClass('open-menu');
    $('#sidenav').css('width', '300px');
    $('#main').css('margin-left', '300px');
}

function closeNav() {
    $('.left-nave-toggle-btn').removeClass('open-menu');
    $('#sidenav').css('width', '0px');
    $('#main').css('margin-left', '0px');
}


//Loader
function run_waitMe(el, num, effect) {
    var rootFolder = $("#BaseUrl").data("baseurl");
    text = '';
    fontSize = '';
    switch (num) {
        case 1:
            maxSize = '';
            textPos = 'vertical';
            break;
        case 2:
            text = '';
            maxSize = 30;
            textPos = 'vertical';
            break;
        case 3:
            maxSize = 30;
            textPos = 'horizontal';
            fontSize = '18px';
            break;
    }
    el.waitMe({
        effect: effect,
        text: text,
        bg: 'rgba(255,255,255,0.7)',
        color: '#000',
        maxSize: maxSize,
        waitTime: -1,
        source: rootFolder + 'Images/loader.gif',
        textPos: textPos,
        fontSize: fontSize,
        onClose: function (el) { }
    });
};

function pagescroll() {
    $('html, body').animate({ scrollTop: 0 }, 'fast');
};