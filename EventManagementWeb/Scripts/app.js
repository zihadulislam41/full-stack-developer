﻿var app = angular.module("app", []);
var baseUrl = $("#BaseUrl").data("baseurl");
var apiUrl = 'http://localhost:24618/';
var version = 1;
angular.module('app', ['ngRoute', 'ngMeta', 'ngCookies']).config(function ($routeProvider, $locationProvider) {
    $routeProvider.caseInsensitiveMatch = true;
    $routeProvider.when('/', {
        redirectTo: function () {
            return '/';
        }
    })
        .when('/home', {
            redirectTo: function () {
                return '/';
            }
        })
        .when('/', {
            templateUrl: baseUrl + '/templates/Event/Home.html?v=' + version,
            controller: 'HomeController',
            data: {
                meta: {
                    'title': 'welcome',
                    'description': 'This is the description shown in Google search results'
                }
            }
        })
        .when('/add-event/:id', {
            templateUrl: baseUrl + '/templates/Event/AddEditEvent.html?v=' + version,
            controller: 'AddEditEventController',
            data: {
                meta: {
                    'title': 'welcome',
                    'description': 'This is the description shown in Google search results'
                }
            }
        })
        .when('/update-event/:id', {
            templateUrl: baseUrl + '/templates/Event/AddEditEvent.html?v=' + version,
            controller: 'AddEditEventController',
            data: {
                meta: {
                    'title': 'welcome',
                    'description': 'This is the description shown in Google search results'
                }
            }
        })
        .when('/register', {
            templateUrl: baseUrl + '/templates/Register/Register.html?v=' + version,
            controller: 'RegisterController',
            data: {
                meta: {
                    'title': 'Register',
                    'description': 'This is the description shown in Google search results'
                }
            }
        })
        .when('/login', {
            templateUrl: baseUrl + '/templates/Login/Login.html?v=' + version,
            controller: 'LoginController',
            data: {
                meta: {
                    'title': 'Login',
                    'description': 'This is the description shown in Google search results'
                },
                disableUpdate: true
            }
        })
        .when('/not-found', {
            templateUrl: baseUrl + '/templates/Error/NotFound.html?v=' + version,
            controller: 'NotFoundController',
            data: {
                meta: {
                    'title': 'Page Not Found',
                    'description': 'This is the description shown in Google search results'
                }
            }
        })
        .otherwise({
            redirectTo: function () {
                return '/not-found';
            }
        });
    $locationProvider.html5Mode({
        enabled: true,
        requireBase: false
    });

})
    //for nav click & page reload
    .directive('a', function ($route, $location) {
        var d = {};
        d.restrict = 'E';
        d.link = function (scope, elem, attrs) {
            // has target
            if ('target' in attrs) return;
            // doesn't have href
            if (!('href' in attrs)) return;
            // href is not the current path
            var href = elem[0].href;
            elem.bind('click', function () {
                if (href !== $location.absUrl()) return;
                $route.reload();
            });
        };
        return d;
    });



