﻿using System.Web;
using System.Web.Optimization;

namespace EventManagementWeb
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/angular").Include(
                                 "~/Scripts/angular-1.6.9.min.js",
                                 "~/Scripts/angular-route.js",
                                 "~/Scripts/angular-cookies.js",
                                 "~/Scripts/ngMeta.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/routing").Include(
                     "~/Scripts/app.js"));

            bundles.Add(new ScriptBundle("~/bundles/js").Include(
                    //basic scripts
                    "~/Scripts/popper.min.js",
                    "~/Scripts/bootstrap.js",
                      //Loader
                      "~/Scripts/waitMe.js",
                      //Moment
                      "~/Scripts/moment.min.js",
                      //Alart
                      "~/Scripts/sweetalert.min.js",
                      //Custom Js
                      "~/Scripts/main.js"
                    ));

            bundles.Add(new ScriptBundle("~/bundles/contoller").Include(
                      //Run
                      "~/templates/App/Run.js",

                      //Services
                      "~/templates/Login/LoginService.js",
                      "~/templates/Register/RegisterService.js",
                      "~/templates/Event/EventService.js",

                      //Controller
                      "~/templates/App/MainController.js",
                      "~/templates/Event/HomeController.js",
                      "~/templates/Login/LoginController.js",
                      "~/templates/Register/RegisterController.js",
                      "~/templates/Event/AddEditEventController.js",
                      "~/templates/Error/NotFoundController.js"
                    ));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                     "~/Content//bootstrap.min.css",
                     "~/font-awesome/css/font-awesome.css",
                     "~/Content/animate.css",
                      "~/Content/waitMe.css",
                      "~/Content/sweetalert.css",
                     "~/Content/custom.css"
                     ));

            BundleTable.EnableOptimizations = false;
        }
    }
}
