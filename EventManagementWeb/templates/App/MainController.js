﻿angular.module('app').controller('MainController', function ($scope, $http, $location, $compile, $route, $rootScope, LoginService) {
    pagescroll();
    $scope.authpanel = true; // default visibility state
    $scope.showAuthPanel = function (show) {
        if (show) openNav();
        else closeNav();
        $scope.authpanel = show;
    };
    //page dedirect
    $scope.gotoPage = function (url) {
        if ($location.url() == url) {
            $route.reload();
            return;
        }
        $location.path(url);
    };
    //Logout
    $scope.Logout = function () {
        LoginService.ClearCredentials();
        $location.path('/login');
    };
    // Set user info
    $scope.setUserInfo = function () {
        var loggedIn = $rootScope.globals.currentUser;
        if (loggedIn) {
            $scope.profile_img = apiUrl + 'UploadedFiles/' + $rootScope.globals.currentUser.image;
            $scope.user_name = $rootScope.globals.currentUser.name;
            $scope.user_email = $rootScope.globals.currentUser.email;
        }
    };
    $scope.setUserInfo();
})