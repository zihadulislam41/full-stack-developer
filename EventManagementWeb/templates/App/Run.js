﻿angular.module('app').run(['$rootScope', '$location', '$cookieStore', '$http', '$routeParams', 'ngMeta',
    function ($rootScope, $location, $cookieStore, $http, $routeParams, ngMeta) {
        ngMeta.init();
        // keep user logged in after page refresh
        $rootScope.globals = $cookieStore.get('globals') || {};
        if ($rootScope.globals.currentUser) {
            $http.defaults.headers.common['Authorization'] = 'Basic ' + $rootScope.globals.currentUser.authdata;
        }

        $rootScope.$on('$locationChangeStart', function (scope, next, current) {
            // redirect to login page if not logged in
            var restrictedPage = $.inArray($location.path(), ['/login', '/register']) === -1;
            var loggedIn = $rootScope.globals.currentUser;
            if (restrictedPage && !loggedIn) {
                $location.path('/login');
            }
        });
    }])