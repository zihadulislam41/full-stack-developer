﻿angular.module('app').controller('RegisterController', function ($scope, $http, $location, $rootScope, RegisterService) {
    pagescroll();
    $scope.imageUrl = apiUrl + 'UploadedFiles/avatar.png';
    $scope.showAuthPanel(false);

    $scope.register = function () {
        $scope.dataLoading = true;
        RegisterService.Register($scope.name, $scope.email, $scope.password, $scope.dob, $('#profile-img').val(), function (response) {
            if (response.success) {
                $location.path('/login');
                $scope.$apply();
            } else {
                $scope.dataLoading = false;
                $scope.$apply();
            }
        });
    };
})