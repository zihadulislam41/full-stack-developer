﻿angular.module('app').factory('RegisterService', ['$http', '$cookieStore', '$rootScope', function ($http, $cookieStore, $rootScope) {
    var service = {};
    /*--------- Use Register--------------*/
    service.Register = function (name, email, password, dob, image, callback) {
        var UserModel = new Object();
        UserModel.FullName = name;
        UserModel.Email = email;
        UserModel.Password = password;
        UserModel.Birthday = moment(dob).format('LL');
        UserModel.ProfileImage = image;
        $.ajax({
            url: apiUrl + 'api/v1/user-register',
            dataType: "json",
            type: "POST",
            data: UserModel,
            success: function (response) {
                Swal.fire({
                    icon: 'success',
                    title: 'Your registration has been successful!',
                    text: "We have successfully registered. Click 'OK' button for login!",
                    showCancelButton: false,
                    confirmButtonText: 'OK',
                    allowOutsideClick: false,
                }).then((result) => {
                    response = new Object();
                    response.success = true;
                    callback(response);
                });
            },
            error: function (response, status, error) {
                if (error == 'Conflict') {
                    Swal.fire({
                        icon: 'warning',
                        title: 'Already Exists',
                        text: 'This email is already taken, Please try another one!',
                        showCancelButton: false,
                        confirmButtonText: 'OK',
                        allowOutsideClick: false
                    })
                }
                //fn_error_handler( request, status, error);
                response = new Object();
                response.success = false;
                callback(response);
            }
        });
    };

    return service;
}])