﻿angular.module('app').controller('AddEditEventController', function ($scope, $http, $location, $rootScope, $routeParams, EventService) {
    pagescroll();
    $scope.showAuthPanel(true);
    $scope.locations = ["Dhaka, Bangladesh", "Delhi, India", "Taxus, USA"];
    $scope.labels = ["Red", "Blue", "Green"];
    $scope.header = "Create New Event";
    $scope.submit_button_text = "Create";
    $scope.eventId = $routeParams.id;

    //If it's update event
    if ($routeParams.id > 0) {
        EventService.GetEventById($routeParams.id, function (response) {
            var event = response.event;
            if (event.EventId == 0) $location.path('/not-found');
            $scope.title = event.Title;
            $scope.description = event.Description;
            $scope.date = new Date(moment(event.Date).format('L'));
            if (event.FromTime != null)
                $scope.fromTime = moment("01-01-20001 " + event.FromTime).format('HH:mm');
            if (event.FromTime != null)
                $scope.toTime = moment("01-01-20001 " + event.ToTime).format('HH:mm');
            $scope.location = event.Location;
            $scope.notify = parseInt(event.Notify);
            $scope.label = event.Label;
            $scope.header = "Update Event";
            $scope.submit_button_text = "Update Changes";
            angular.forEach($scope.form.$error.required, function (field) {
                field.$setDirty();
            });
            $scope.$apply();
        });
    }
    //Save Event
    $scope.saveEvent = function () {
        $scope.dataLoading = true;
        var Event = new Object();
        Event.EventId = $scope.eventId;
        Event.Title = $scope.title;
        Event.Description = $scope.description;
        Event.Date = moment($scope.date).format('LL');
        Event.FromTime = $('#fromTime').val();
        Event.ToTime = $('#toTime').val();
        Event.Location = $scope.location;
        Event.Notify = $scope.notify;
        Event.Label = $scope.label;
        Event.UserId = $rootScope.globals.currentUser.id;
        EventService.AddEditEvent(Event, function (response) {
            if (response.success) {
                $location.path('/');
                $scope.dataLoading = false;
                $scope.$apply();
            } else {
                $scope.dataLoading = false;
                $scope.$apply();
            }
        });
    };
})