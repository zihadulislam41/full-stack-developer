﻿angular.module('app').factory('EventService', ['$http', '$cookieStore', '$rootScope', function ( $http, $cookieStore, $rootScope) {
    var service = {};
    /*--------- Load All Events--------------*/
    service.LoadAllEvents = function (callback) {
        $.ajax({
            url: apiUrl + 'api/v1/get-events-by-user',
            dataType: "json",
            headers: {
                'Authorization': 'Bearer ' + $rootScope.globals.currentUser.token
            },
            type: "GET",
            data: { userId: $rootScope.globals.currentUser.id },
            success: function (response) {
                callback(response);
            },
            error: function (response, status, error) {
                //fn_error_handler( request, status, error);
                response = new Object();
                response.success = false;
                callback(response);
            }
        });
    };

    /*--------- Get Event By Id--------------*/
    service.GetEventById = function (id, callback) {
        $.ajax({
            url: apiUrl + 'api/v1/get-event-by-id',
            dataType: "json",
            headers: {
                'Authorization': 'Bearer ' + $rootScope.globals.currentUser.token
            },
            type: "GET",
            data: { userId: $rootScope.globals.currentUser.id, eventId: id },
            success: function (response) {
                callback(response);
            },
            error: function (response, status, error) {
                //fn_error_handler( request, status, error);
                response = new Object();
                response.success = false;
                callback(response);
            }
        });
    };

    /*--------- Add/Edit Event--------------*/
    service.AddEditEvent = function (eventObj, callback) {
        $.ajax({
            url: apiUrl + 'api/v1/save-event',
            dataType: "json",
            headers: {
                'Authorization': 'Bearer ' + $rootScope.globals.currentUser.token
            },
            type: "POST",
            data: eventObj,
            success: function (response) {
                Swal.fire({
                    icon: 'success',
                    title: 'Event creation has been successful!',
                    text: "Event successfully created!",
                    showCancelButton: false,
                    confirmButtonText: 'OK',
                    allowOutsideClick: false,
                }).then((result) => {
                    response = new Object();
                    response.success = true;
                    callback(response);
                });
            },
            error: function (response, status, error) {
                Swal.fire({
                    icon: 'warning',
                    title: 'Faild!',
                    text: 'Internal Server Error!',
                    showCancelButton: false,
                    confirmButtonText: 'OK',
                    allowOutsideClick: false
                })
                //fn_error_handler( request, status, error);
                response = new Object();
                response.success = false;
                callback(response);
            }
        });
    };

    /*--------- Delete Event--------------*/
    service.DeleteEvent = function (id, callback) {
        $.ajax({
            url: apiUrl + 'api/v1/delete-event',
            dataType: "json",
            headers: {
                'Authorization': 'Bearer ' + $rootScope.globals.currentUser.token
            },
            type: "GET",
            data: { id: id },
            success: function (response) {
                callback(response);
            },
            error: function (response, status, error) {
                //fn_error_handler( request, status, error);
                response = new Object();
                response.success = false;
                callback(response);
            }
        });
    };

    return service;
}])