﻿angular.module('app').controller('HomeController', function ($scope, $http, $location, $route, $rootScope, EventService) {
    pagescroll();
    $scope.showAuthPanel(true);
    $scope.setUserInfo();
    //Load Events
    $scope.loadAllEvents = function () {
        EventService.LoadAllEvents(function (response) {
            var events = this;
            events.date = response.events;
            var groupByDate = function groupByDate(data) {
                var grouped = {};
                angular.forEach(data, function (d) {
                    var actualDay = moment(d.Date).format('LL');
                    if (!grouped[actualDay]) {
                        grouped[actualDay] = [];
                    }
                    d.FromTime = moment("01-01-0001 " + d.FromTime).format('LT');
                    grouped[actualDay].push(d);
                });

                return grouped;
            };
            events.groupedData = groupByDate(events.date);
            $scope.events = events;
            $scope.$apply();
        });
    };
    $scope.loadAllEvents();
    //View Event
    $scope.viewEvent = function (id) {
        EventService.GetEventById(id, function (response) {
            var event = response.event;
            if (event.EventId == 0) $location.path('/not-found');
            $scope.title = event.Title;
            $scope.description = event.Description;
            $scope.date = moment(event.Date).format('L');
            if (event.FromTime != null)
                $scope.fromTime = moment("01-01-20001 " + event.FromTime).format('hh:mm a');
            if (event.FromTime != null)
                $scope.toTime = moment("01-01-20001 " + event.ToTime).format('hh:mm a');
            $scope.location = event.Location;
            $scope.$apply();
            $('#eventViewModal').modal('show');
        });
    }
    //Delete Event
    $scope.deleteEvent = function (id) {
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            allowOutsideClick: false,
            confirmButtonText: 'Delete',
            cancelButtonText: 'Not Now'
        }).then((result) => {
            if (result.value) {
                EventService.DeleteEvent(id, function (response) {
                    $route.reload();
                    $scope.dataLoading = false;
                    $scope.$apply();
                })
            }
        });
    }
});