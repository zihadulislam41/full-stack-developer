﻿angular.module('app').controller('LoginController', function ($scope, $http, $location, $route, $routeParams, ngMeta, LoginService) {
    pagescroll();
    $scope.showAuthPanel(false);
    // reset login status
    LoginService.ClearCredentials();
    // user login
    $scope.login = function () {
        $scope.dataLoading = true;
        LoginService.Login($scope.email, $scope.password, function (response) {
            if (response.token != 'Invalid') {
                var user = response.user;
                LoginService.SetCredentials(user, response.token);
                $location.path('/');
                $scope.$apply();
            } else {
                $scope.dataLoading = false;
                $scope.$apply();
            }
        });
    };
})