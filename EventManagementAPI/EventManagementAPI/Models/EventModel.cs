﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EventManagementAPI.Models
{
    public class EventModel
    {
        public int EventId { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime Date { get; set; }
        public TimeSpan FromTime { get; set; }
        public TimeSpan ToTime { get; set; }
        public string Location { get; set; }
        public string Notify { get; set; }
        public string Label { get; set; }
        public bool IsDeleted { get; set; }
        public int UserId { get; set; }
    }
}