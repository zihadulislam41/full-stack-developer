﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EventManagementAPI.Models
{
    public class UserModel
    {
        public int UserId { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string FullName { get; set; }
        public DateTime Birthday { get; set; }
        public string ProfileImage { get; set; }
    }
}