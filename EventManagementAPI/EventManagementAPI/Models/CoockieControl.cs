﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;

namespace EventManagementAPI.Models
{
    public class CoockieControl
    {

        public static string GetCookiesCart()
        {
            string _tempId = "";
            try
            {
                HttpCookie _cookies = HttpContext.Current.Request.Cookies["cart"];
                if (_cookies != null)
                {
                    string _value = _cookies.Value;
                    var _bytes = Convert.FromBase64String(_value);
                    var _decryptedValue = MachineKey.Unprotect(_bytes, "CartTempId");
                    _tempId = Encoding.UTF8.GetString(_decryptedValue);
                }
            }
            catch (Exception)
            {
                _tempId = "";
            }
            if (_tempId == "")
            {
                _tempId = DateTime.Now.ToString("yyyyMMddHHmmss");
                var _cookieText = Encoding.UTF8.GetBytes(_tempId);
                var _encryptedValue = Convert.ToBase64String(MachineKey.Protect(_cookieText, "CartTempId"));

                HttpCookie tempCookie = new HttpCookie("cart", _encryptedValue);
                tempCookie.Expires.AddDays(30);
                var request = HttpContext.Current.Response;
                request.Cookies.Add(tempCookie);
            }

            return _tempId;
        }
    }
}