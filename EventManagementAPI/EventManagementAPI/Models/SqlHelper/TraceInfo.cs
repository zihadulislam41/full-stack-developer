﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EventManagementAPI.Models.SqlHelper
{
    public class TraceInfo
    {
        public static bool IsTraceOn { get; set; }

        public static List<string> Procedures
        {
            get;
            set;
        }
    }
}