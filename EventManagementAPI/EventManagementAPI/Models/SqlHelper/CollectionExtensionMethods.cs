﻿using Microsoft.SqlServer.Server;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace EventManagementAPI.Models.SqlHelper
{
    public static class CollectionExtensionMethods
    {
        /// <summary>
        /// Converts List<int> to an Table Value type has one int Column named id
        /// </summary>
        /// <param name="values"></param>
        /// <returns></returns>
        public static object ToTableType(this IEnumerable<int> values)
        {
            if ((values == null) || (values.Count() == 0))
            {
                return IntCollection.EmptyTable;
            }
            else
            {
                IntCollection collection = new IntCollection();
                collection.AddRange(values);
                return collection;
            }
        }


        public static object ToTableTypeLong(this IEnumerable<Int64> values)
        {
            if ((values == null) || (values.Count() == 0))
            {
                return IntCollection.EmptyTable;
            }
            else
            {
                IntCollectionLong collection = new IntCollectionLong();
                collection.AddRange(values);
                return collection;
            }
        }

        #region Collection Helper Class (SqlDataRecord Enumerator class)
        /// <summary>
        /// Define IntCollection Class
        /// </summary>
        internal class IntCollection : List<int>, IEnumerable<SqlDataRecord>
        {
            static IntCollection()
            {
                dataTable = new DataTable();
                dataTable.Columns.Add("id", typeof(int));
            }
            private static DataTable dataTable;
            public static DataTable EmptyTable
            {
                get
                {
                    return dataTable;
                }
            }

            IEnumerator<SqlDataRecord> IEnumerable<SqlDataRecord>.GetEnumerator()
            {
                var row = new SqlDataRecord(new SqlMetaData("id", SqlDbType.Int));
                foreach (int val in this)
                {
                    row.SetInt32(0, val);
                    yield return row;
                }
            }

        }

        internal class IntCollectionLong : List<Int64>, IEnumerable<SqlDataRecord>
        {
            static IntCollectionLong()
            {
                dataTable = new DataTable();
                dataTable.Columns.Add("id", typeof(int));
            }
            private static DataTable dataTable;
            public static DataTable EmptyTable
            {
                get
                {
                    return dataTable;
                }
            }

            IEnumerator<SqlDataRecord> IEnumerable<SqlDataRecord>.GetEnumerator()
            {
                var row = new SqlDataRecord(new SqlMetaData("id", SqlDbType.Int));
                foreach (int val in this)
                {
                    row.SetInt32(0, val);
                    yield return row;
                }
            }

        }
        #endregion
    }
}