﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace EventManagementAPI.App_Start
{
    public class WebConfig
    {
        public static string Connectionstring => ConfigurationManager.AppSettings["connectionString"];
    }
}