﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace EventManagementAPI.Controllers
{
    public class ApiFileUploadController : ApiController
    {
        /// <summary>
        /// Image file upload API.
        /// </summary>
        /// <returns>Image Name</returns>
        [Route("api/v1/image-upload")]
        [HttpPost]
        public JObject ImageUpload()
        {
            try
            {
                int _image_id = 0;
                string _fileName = "";
                if (HttpContext.Current.Request.Files.AllKeys.Any())
                {
                    // Get the uploaded image from the Files collection
                    var _httpPostedFile = HttpContext.Current.Request.Files["UploadedImage"];

                    if (_httpPostedFile != null)
                    {
                        // Validate the uploaded image(optional)
                        var extention = Path.GetExtension(_httpPostedFile.FileName);
                        if (extention.ToLower() == ".png" || extention.ToLower() == ".jpg" || extention.ToLower() == ".jpeg" || extention.ToLower() == ".gif")
                        {
                            // Get the complete file path
                            _fileName = UploadFile(_httpPostedFile, 300, 500);
                         
                        }

                    }
                }
                JObject jsonObject = new JObject(
                    new JProperty("image_name", _fileName)
                 );
                return jsonObject;
            }
            catch (Exception)
            {
                return null;
            }
        }
        [HttpPost]
        public string UploadFile(HttpPostedFile file, int width, int height)
        {

            string _FileName = "";
            string _path = "";
            string _imageUrls = "";
            try
            {
                if (file.ContentLength > 0)
                {
                    _FileName = Path.GetFileName(DateTime.Now.ToBinary() + "-" + file.FileName);
                    _path = Path.Combine(HttpContext.Current.Server.MapPath("~/UploadedFiles"), _FileName);
                    byte[] _fileData = null;
                    using (var _binaryReader = new BinaryReader(file.InputStream))
                    {
                        _fileData = _binaryReader.ReadBytes(file.ContentLength);
                    }

                    HandleImageUpload(_fileData, _path, width, height);

                    _imageUrls = _FileName;
                }
                return _imageUrls;
            }
            catch
            {
                return string.Empty;
            }
        }
        private void HandleImageUpload(byte[] binaryImage, string path, int width, int height)
        {
            Image _img = RezizeImage(Image.FromStream(BytearrayToStream(binaryImage)), width, height);
            _img.Save(path, System.Drawing.Imaging.ImageFormat.Png);
        }
        private Image RezizeImage(Image img, int maxWidth, int maxHeight)
        {
            if (img.Height < maxHeight && img.Width < maxWidth) return img;
            using (img)
            {
                Double xRatio = (double)img.Width / maxWidth;
                Double yRatio = (double)img.Height / maxHeight;
                Double ratio = Math.Max(xRatio, yRatio);
                int nnx = (int)Math.Floor(img.Width / ratio);
                int nny = (int)Math.Floor(img.Height / ratio);
                Bitmap cpy = new Bitmap(nnx, nny, PixelFormat.Format32bppArgb);
                using (Graphics gr = Graphics.FromImage(cpy))
                {
                    gr.Clear(Color.Transparent);

                    // This is said to give best quality when resizing images
                    gr.InterpolationMode = InterpolationMode.HighQualityBicubic;

                    gr.DrawImage(img,
                        new Rectangle(0, 0, nnx, nny),
                        new Rectangle(0, 0, img.Width, img.Height),
                        GraphicsUnit.Pixel);
                }
                return cpy;
            }

        }
        private MemoryStream BytearrayToStream(byte[] arr)
        {
            return new MemoryStream(arr, 0, arr.Length);
        }
    }
}
