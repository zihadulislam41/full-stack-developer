﻿using EventManagementAPI.DataLayer;
using EventManagementAPI.Models;
using EventManagementAPI.ServiceLayer;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace EventManagementAPI.Controllers
{
    public class ApiEventController : ApiController
    {
        private readonly EventService _eventService;
        /// <summary>
        /// All services will be diclare here into the constructor.
        /// </summary>
        public ApiEventController()
        {
            _eventService = new EventService();
        }

        /// <summary>
        /// Get all events data for specific user API. Token based autorization.
        /// </summary>
        /// <param name="userId"></param>
        /// <returns>Events data as Json object array</returns>
        [Authorize]
        [HttpGet]
        [Route("api/v1/get-events-by-user")]
        public IHttpActionResult GetEventsByUser(int userId)
        {
            try
            {
                List<EventModel> _eventList = new List<EventModel>();
                _eventList = _eventService.GetEventsByUser(userId);
                JObject jsonObject = new JObject(
                    new JProperty("events", JArray.Parse(JsonConvert.SerializeObject(_eventList, Formatting.None)))
                );
                return Ok(jsonObject);
            }
            catch (Exception)
            {
                //Error log create here
                return InternalServerError();
            }
        }

        /// <summary>
        /// Get event data for specific user API. Token based autorization.
        /// </summary>
        /// <param name="userId and eventId"></param>
        /// <returns>Event data as Json object</returns>
        [Authorize]
        [HttpGet]
        [Route("api/v1/get-event-by-id")]
        public IHttpActionResult GetEventById(int userId, int eventId)
        {
            try
            {
                EventModel _event = _eventService.GetEventById(eventId);
                if (_event == null) _event = new EventModel();
                else if(_event.UserId!=userId)
                {
                    _event = new EventModel();
                }
                JObject jsonObject = new JObject(
                    new JProperty("event", JObject.Parse(JsonConvert.SerializeObject(_event, Formatting.None)))
                );
                return Ok(jsonObject);
            }
            catch (Exception)
            {
                //Error log create here
                return InternalServerError();
            }
        }

        /// <summary>
        /// Save event data API. Token based autorization.
        /// </summary>
        /// <param name="EventModel"></param>
        /// <returns>Event data as Json object</returns>
        [Authorize]
        [HttpPost]
        [Route("api/v1/save-event")]
        public IHttpActionResult SaveEvent(EventModel _event)
        {
            try
            {
                if (_event.EventId > 0)
                {
                    _event = _eventService.UpdateEvent(_event);
                }
                else if (_event.EventId == 0)
                {
                    _event = _eventService.InsertEvent(_event);
                }
                JObject jsonObject = new JObject(
                    new JProperty("event", JObject.Parse(JsonConvert.SerializeObject(_event, Formatting.None)))
                );
                return Ok(jsonObject);
            }
            catch (Exception)
            {
                //Error log create here
                return InternalServerError();
            }
        }

        /// <summary>
        /// Delete event API. Token based autorization.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>Message</returns>
        [Authorize]
        [HttpGet]
        [Route("api/v1/delete-event")]
        public IHttpActionResult DeleteEvent(int id)
        {
            try
            {
                _eventService.DeleteEvent(id);
                JObject jsonObject = new JObject(
                   new JProperty("message", "success")
               );
                return Ok(jsonObject);
            }
            catch (Exception)
            {
                //Error log create here
                return InternalServerError();
            }
        }
    }
}
