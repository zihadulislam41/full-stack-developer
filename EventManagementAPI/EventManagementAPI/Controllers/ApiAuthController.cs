﻿using EventManagementAPI.Models;
using EventManagementAPI.ServiceLayer;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web;
using System.Web.Http;

namespace EventManagementAPI.Controllers
{
    public class ApiAuthController : ApiController
    {
       
        private readonly UserService _userService;
        /// <summary>
        /// All services will be diclare here into the constructor.
        /// </summary>
        public ApiAuthController()
        {
            _userService = new UserService();
        }

        /// <summary>
        /// User registration API
        /// </summary>
        /// <param name="UserModel"></param>
        /// <returns>If Exists return Conflict code 409 otherwise return registred user object</returns>
        [Route("api/v1/user-register")]
        [HttpPost]
        public IHttpActionResult UserRegister(UserModel user)
        {
            try
            {
                UserModel _userObj = _userService.GetUserByEmail(user.Email);
                if (_userObj.UserId > 0)
                {
                    return Conflict();
                }
                _userObj = new UserModel
                {
                    Email = user.Email,
                    Password = user.Password,
                    FullName = user.FullName,
                    Birthday = user.Birthday,
                    ProfileImage = user.ProfileImage
                };
                _userObj = _userService.InsertUser(_userObj);
                JObject jsonObject = new JObject(
                        new JProperty("user", JObject.Parse(JsonConvert.SerializeObject(_userObj, Formatting.None)))
                 );
                return Ok(jsonObject);
            }
            catch (Exception)
            {
                //Error log create here
                return InternalServerError();
            }
        }

        /// <summary>
        /// User login API
        /// </summary>
        /// <param name="UserModel"></param>
        /// <returns>If invalid youser return invalid token otherwise return registred user object with token</returns>
        [Route("api/v1/user-login")]
        [HttpPost]
        public IHttpActionResult Login(UserModel user)
        {
            try
            {
                string _accessToken = "Invalid";
                string _baseAddress = HttpContext.Current.Request.Url.GetLeftPart(UriPartial.Authority);
                using (var client = new HttpClient())
                {
                    var form = new Dictionary<string, string>
                    {
                        {"grant_type", "password"},
                        {"username", user.Email},
                        {"password", user.Password},
                    };
                    var tokenResponse = client.PostAsync(_baseAddress + "/token", new FormUrlEncodedContent(form)).Result;  
                    var token = tokenResponse.Content.ReadAsAsync<Token>(new[] { new JsonMediaTypeFormatter() }).Result;
                    if (string.IsNullOrEmpty(token.Error))
                    {
                        _accessToken = token.AccessToken;
                        user = _userService.GetUserByEmailAndPassword(user.Email, user.Password);
                    }
                    else
                    {
                        user = new UserModel();
                    }
                    JObject jsonObject = new JObject(
                           new JProperty("user", JObject.Parse(JsonConvert.SerializeObject(user, Formatting.None))),
                           new JProperty("token", _accessToken)
                       );
                    return Ok(jsonObject);
                }
            }
            catch (Exception ex)
            {
                //Error log create here
                return InternalServerError();
            }
        }
    }
}
