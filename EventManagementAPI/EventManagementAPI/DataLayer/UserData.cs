﻿using EventManagementAPI.App_Start;
using EventManagementAPI.Models;
using EventManagementAPI.Models.SqlHelper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace EventManagementAPI.DataLayer
{
    public class UserData
    {
        /// <summary>
        /// Get User
        /// </summary>
        /// <param name=""></param>
        /// <returns>UserModel object List</returns>
        public List<UserModel> GetUsers()
        {
            List<UserModel> _list = new List<UserModel>();
            using (SqlDataReader dr = SqlHelper.ExecuteReader(WebConfig.Connectionstring, "pr_users_get"))
            {
                while (dr.Read())
                {
                    UserModel _user = new UserModel();
                    _user.UserId = Convert.ToInt32(dr["UserId"].ToString());
                    _user.Email = dr["Email"].ToString();
                    _user.Password = dr["Password"].ToString();
                    _user.FullName = dr["FullName"].ToString();
                    if (!string.IsNullOrEmpty(dr["Birthday"].ToString()))
                        _user.Birthday = Convert.ToDateTime(dr["Birthday"].ToString());
                    _user.ProfileImage = dr["ProfileImage"].ToString();
                    _list.Add(_user);
                }
            }
            return _list;
        }

        /// <summary>
        /// Get User by Id.
        /// </summary>
        /// <param name="id"></param>
        /// <returns>UserModel</returns>
        public UserModel GetByUserId(int id)
        {
            List<int> _ids = new List<int>();
            _ids.Add(id);
            var user = GetUserList(_ids).FirstOrDefault();
            return user;
        }

        /// <summary>
        /// Get User List.
        /// </summary>
        /// <param name="lstids"></param>
        /// <returns>ICollection of UserModel</returns>
        public static ICollection<UserModel> GetUserList(List<int> lstids)
        {
            List<UserModel> _list = new List<UserModel>();

            SqlParameter[] prms = SqlHelper.GetParams(WebConfig.Connectionstring, "pr_user_get_list");
            prms[0].Value = lstids.ToTableType();

            using (SqlDataReader dr = SqlHelper.ExecuteReader(WebConfig.Connectionstring, "pr_user_get_list", prms))
            {
                while (dr.Read())
                {
                    UserModel user = new UserModel();
                    user.UserId = Convert.ToInt32(dr["UserId"].ToString());
                    user.Email = dr["Email"].ToString();
                    user.Password = dr["Password"].ToString();
                    user.FullName = dr["FullName"].ToString();
                    if (!string.IsNullOrEmpty(dr["Birthday"].ToString()))
                        user.Birthday = Convert.ToDateTime(dr["Birthday"].ToString());
                    user.ProfileImage = dr["ProfileImage"].ToString();
                    _list.Add(user);
                }
            }
            return _list;
        }

        /// <summary>
        /// Get User by Email
        /// </summary>
        /// <param name="email"></param>
        /// <returns>UserModel</returns>
        public UserModel GetUserByEmail(string email = "")
        {
            UserModel _user = new UserModel();

            SqlParameter[] prms = SqlHelper.GetParams(WebConfig.Connectionstring, "pr_user_get_by_email");
            prms[0].Value = email;

            using (SqlDataReader dr = SqlHelper.ExecuteReader(WebConfig.Connectionstring, "pr_user_get_by_email", prms))
            {
                while (dr.Read())
                {
                    _user.UserId = Convert.ToInt32(dr["UserId"].ToString());
                    _user.Email = dr["Email"].ToString();
                    _user.Password = dr["Password"].ToString();
                    _user.FullName = dr["FullName"].ToString();
                    if (!string.IsNullOrEmpty(dr["Birthday"].ToString()))
                        _user.Birthday = Convert.ToDateTime(dr["Birthday"].ToString());
                    _user.ProfileImage = dr["ProfileImage"].ToString();
                }
            }
            return _user;
        }

        /// <summary>
        /// Get User for Login.
        /// </summary>
        /// <param name="email and password"></param>
        /// <returns>UserModel</returns>
        public UserModel GetUserByEmailAndPassword(string email = "", string password = "")
        {
            UserModel _user = new UserModel();

            SqlParameter[] prms = SqlHelper.GetParams(WebConfig.Connectionstring, "pr_user_get_by_email_password");
            prms[0].Value = email;
            prms[1].Value = password;

            using (SqlDataReader dr = SqlHelper.ExecuteReader(WebConfig.Connectionstring, "pr_user_get_by_email_password", prms))
            {
                while (dr.Read())
                {
                    _user.UserId = Convert.ToInt32(dr["UserId"].ToString());
                    _user.Email = dr["Email"].ToString();
                    _user.Password = dr["Password"].ToString();
                    _user.FullName = dr["FullName"].ToString();
                    if (!string.IsNullOrEmpty(dr["Birthday"].ToString()))
                        _user.Birthday = Convert.ToDateTime(dr["Birthday"].ToString());
                    _user.ProfileImage = dr["ProfileImage"].ToString();
                }
            }
            return _user;
        }

        /// <summary>
        /// Insert User Info.
        /// </summary>
        /// <param name="UserModel"></param>
        /// <returns>UserModel</returns>
        public UserModel InsertUser(UserModel user)
        {
            int userId = 0;
            SqlParameter[] prms = SqlHelper.GetParams(WebConfig.Connectionstring, "pr_user_set");
            prms[0].Value = user.UserId;
            prms[1].Value = user.Email;
            prms[2].Value = user.Password;
            prms[3].Value = user.FullName;
            prms[4].Value = user.Birthday;
            prms[5].Value = user.ProfileImage;
            object obj = SqlHelper.ExecuteScalar(WebConfig.Connectionstring, "pr_user_set", prms);
            if (obj != null)
            {
                userId = Convert.ToInt32(obj);
                user.UserId = userId;
            }
            return user;
        }

    }
}