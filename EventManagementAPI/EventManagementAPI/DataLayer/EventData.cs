﻿using EventManagementAPI.App_Start;
using EventManagementAPI.Models;
using EventManagementAPI.Models.SqlHelper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace EventManagementAPI.DataLayer
{
    public class EventData
    {
        /// <summary>
        /// Get Event By User
        /// </summary>
        /// <param name="userId"></param>
        /// <returns>EventModel object List</returns>
        public List<EventModel> GetEventsByUser(int userId)
        {
            List<EventModel> _list = new List<EventModel>();
            SqlParameter[] prms = SqlHelper.GetParams(WebConfig.Connectionstring, "pr_event_get_by_user");
            prms[0].Value = userId;
            using (SqlDataReader dr = SqlHelper.ExecuteReader(WebConfig.Connectionstring, "pr_event_get_by_user", prms))
            {
                while (dr.Read())
                {
                    EventModel _event = new EventModel();
                    _event.EventId = Convert.ToInt32(dr["EventId"].ToString());
                    _event.Title = dr["Title"].ToString();
                    _event.Description = dr["Description"].ToString();
                    if (!string.IsNullOrEmpty(dr["Date"].ToString()))
                        _event.Date = Convert.ToDateTime(dr["Date"].ToString());
                    if (!string.IsNullOrEmpty(dr["FromTime"].ToString()))
                        _event.FromTime = TimeSpan.Parse(dr["FromTime"].ToString());
                    if (!string.IsNullOrEmpty(dr["ToTime"].ToString()))
                        _event.ToTime = TimeSpan.Parse(dr["ToTime"].ToString());
                    _event.Location = dr["Location"].ToString();
                    _event.Notify = dr["Notify"].ToString();
                    _event.Label = dr["Label"].ToString();
                    _list.Add(_event);
                }
            }
            return _list;
        }

        /// <summary>
        /// Get Event by Id
        /// </summary>
        /// <param name="Id"></param>
        /// <returns>EventModel</returns>
        public EventModel GetByEventId(int id)
        {
            List<int> _ids = new List<int>();
            _ids.Add(id);
            var user = GetEventList(_ids).FirstOrDefault();
            return user;
        }

        /// <summary>
        /// Get Event List
        /// </summary>
        /// <param name="lstids"></param>
        /// <returns>ICollection of EventModel</returns>
        public static ICollection<EventModel> GetEventList(List<int> lstids)
        {
            List<EventModel> _list = new List<EventModel>();

            SqlParameter[] prms = SqlHelper.GetParams(WebConfig.Connectionstring, "pr_event_get_list");
            prms[0].Value = lstids.ToTableType();

            using (SqlDataReader dr = SqlHelper.ExecuteReader(WebConfig.Connectionstring, "pr_event_get_list", prms))
            {
                while (dr.Read())
                {
                    EventModel _event = new EventModel();
                    _event.EventId = Convert.ToInt32(dr["EventId"].ToString());
                    _event.Title = dr["Title"].ToString();
                    _event.Description = dr["Description"].ToString();
                    if (!string.IsNullOrEmpty(dr["Date"].ToString()))
                        _event.Date = Convert.ToDateTime(dr["Date"].ToString());
                    if (!string.IsNullOrEmpty(dr["FromTime"].ToString()))
                        _event.FromTime = TimeSpan.Parse(dr["FromTime"].ToString());
                    if (!string.IsNullOrEmpty(dr["ToTime"].ToString()))
                        _event.ToTime = TimeSpan.Parse(dr["ToTime"].ToString());
                    _event.Location = dr["Location"].ToString();
                    _event.Notify = dr["Notify"].ToString();
                    _event.Label = dr["Label"].ToString();
                    _event.UserId = Convert.ToInt32(dr["UserId"].ToString());
                    _list.Add(_event);
                }
            }
            return _list;
        }

        /// <summary>
        /// Insert Event
        /// </summary>
        /// <param name="EventModel"></param>
        /// <returns>EventModel</returns>
        public EventModel InsertEvent(EventModel _event)
        {
            int eventId = 0;
            SqlParameter[] prms = SqlHelper.GetParams(WebConfig.Connectionstring, "pr_event_set");
            prms[0].Value = _event.EventId;
            prms[1].Value = _event.Title;
            prms[2].Value = _event.Description;
            prms[3].Value = _event.Date;
            prms[4].Value = _event.FromTime;
            prms[5].Value = _event.ToTime;
            prms[6].Value = _event.Location;
            prms[7].Value = _event.Notify;
            prms[8].Value = _event.Label;
            prms[9].Value = _event.UserId;
            object obj = SqlHelper.ExecuteScalar(WebConfig.Connectionstring, "pr_event_set", prms);
            if (obj != null)
            {
                eventId = Convert.ToInt32(obj);
                _event.EventId = eventId;
            }
            return _event;
        }

        /// <summary>
        /// Update Event
        /// </summary>
        /// <param name="EventModel"></param>
        /// <returns>EventModel</returns>
        public EventModel UpdateEvent(EventModel _event)
        {
            SqlParameter[] prms = SqlHelper.GetParams(WebConfig.Connectionstring, "pr_event_update");
            prms[0].Value = _event.EventId;
            prms[1].Value = _event.Title;
            prms[2].Value = _event.Description;
            prms[3].Value = _event.Date;
            prms[4].Value = _event.FromTime;
            prms[5].Value = _event.ToTime;
            prms[6].Value = _event.Location;
            prms[7].Value = _event.Notify;
            prms[8].Value = _event.Label;
            prms[9].Value = _event.IsDeleted;

            SqlHelper.ExecuteNonQuery(WebConfig.Connectionstring, "pr_event_update", prms);
            return _event;
        }

        /// <summary>
        /// Delete Event
        /// </summary>
        /// <param name="Id"></param>
        public void DeleteEvent(int id)
        {
            var _event = GetByEventId(id);
            _event.IsDeleted = true;
            UpdateEvent(_event);
        }
    }
}