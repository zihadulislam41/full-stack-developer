﻿using EventManagementAPI.DataLayer;
using EventManagementAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EventManagementAPI.ServiceLayer
{
    public class EventService
    {
        private readonly EventData _eventData;
        /// <summary>
        /// Data objects will be diclare here into the constructor.
        /// </summary>
        public EventService()
        {
            _eventData = new EventData();
        }

        /// <summary>
        /// Get Event By User
        /// </summary>
        /// <param name="userId"></param>
        /// <returns>EventModel object List</returns>
        public List<EventModel> GetEventsByUser(int userId)
        {
            try
            {
                var events = _eventData.GetEventsByUser(userId);
                return events;
            }
            catch(Exception ex)
            {
                //create error log;
                return new List<EventModel>();
            }
        }

        /// <summary>
        /// Insert Event
        /// </summary>
        /// <param name="EventModel"></param>
        /// <returns>EventModel</returns>
        public EventModel InsertEvent(EventModel _event)
        {
            try
            {
                return _eventData.InsertEvent(_event);
            }
            catch (Exception ex)
            {
                //create error log;
                return new EventModel();
            }
        }

        /// <summary>
        /// Update Event
        /// </summary>
        /// <param name="EventModel"></param>
        /// <returns>EventModel</returns>
        public EventModel UpdateEvent(EventModel _event)
        {
            try
            {
                return _eventData.UpdateEvent(_event);
            }
            catch (Exception ex)
            {
                //create error log;
                return new EventModel();
            }
        }

        /// <summary>
        /// Get Event by Id
        /// </summary>
        /// <param name="Id"></param>
        /// <returns>EventModel</returns>
        public EventModel GetEventById(int Id)
        {
            try
            {
                return _eventData.GetByEventId(Id);
            }
            catch (Exception ex)
            {
                //create error log;
                return new EventModel();
            }
        }

        /// <summary>
        /// Delete Event
        /// </summary>
        /// <param name="Id"></param>
        public void DeleteEvent(int id)
        {
            try
            {
                _eventData.DeleteEvent(id);
            }
            catch (Exception ex)
            {
                //create error log;
            }
        }
    }
}