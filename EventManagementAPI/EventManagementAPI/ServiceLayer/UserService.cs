﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using EventManagementAPI.DataLayer;
using EventManagementAPI.Models;

namespace EventManagementAPI.ServiceLayer
{
    public class UserService
    {
        private readonly UserData _userData;
        /// <summary>
        /// Data objects will be diclare here into the constructor.
        /// </summary>
        public UserService()
        {
            _userData = new UserData();
        }

        /// <summary>
        /// Get User
        /// </summary>
        /// <param name=""></param>
        /// <returns>UserModel object List</returns>
        public List<UserModel> GetUsers()
        {
            try
            {
                var users = _userData.GetUsers();
                return users;
            }
            catch (Exception ex)
            {
                //create error log;
                return new List<UserModel>();
            }
        }

        /// <summary>
        /// Get User by Email
        /// </summary>
        /// <param name="email"></param>
        /// <returns>UserModel</returns>
        public UserModel GetUserByEmail(string email)
        {
            try
            {
                var user = _userData.GetUserByEmail(email);
                return user;
            }
            catch (Exception ex)
            {
                //create error log;
                return new UserModel();
            }
        }

        /// <summary>
        /// Insert User Info.
        /// </summary>
        /// <param name="UserModel"></param>
        /// <returns>UserModel</returns>
        public UserModel InsertUser(UserModel user)
        {
            try
            {
                return _userData.InsertUser(user);
            }
            catch (Exception ex)
            {
                //create error log;
                return new UserModel();
            }
        }

        /// <summary>
        /// Get User for Login.
        /// </summary>
        /// <param name="email and password"></param>
        /// <returns>UserModel</returns>
        public UserModel GetUserByEmailAndPassword(string email = "", string password = "")
        {
            try
            {
                return _userData.GetUserByEmailAndPassword(email, password);
            }
            catch (Exception ex)
            {
                //create error log;
                return new UserModel();
            }
        }
    }
}