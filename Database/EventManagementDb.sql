USE [EvenManagementDb]
GO
/****** Object:  UserDefinedTableType [dbo].[tt_int]    Script Date: 9/4/2020 5:12:30 PM ******/
CREATE TYPE [dbo].[tt_int] AS TABLE(
	[id] [int] NULL
)
GO
/****** Object:  Table [dbo].[Event]    Script Date: 9/4/2020 5:12:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Event](
	[EventId] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](250) NULL,
	[Description] [nvarchar](max) NULL,
	[Date] [date] NULL,
	[FromTime] [time](7) NULL,
	[ToTime] [time](7) NULL,
	[Location] [nvarchar](150) NULL,
	[Notify] [nvarchar](150) NULL,
	[Label] [varchar](50) NULL,
	[IsDeleted] [bit] NULL,
	[UserId] [int] NULL,
 CONSTRAINT [PK_Event] PRIMARY KEY CLUSTERED 
(
	[EventId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User]    Script Date: 9/4/2020 5:12:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[UserId] [int] IDENTITY(1,1) NOT NULL,
	[Email] [nvarchar](550) NULL,
	[Password] [nvarchar](150) NULL,
	[FullName] [nvarchar](250) NULL,
	[Birthday] [date] NULL,
	[ProfileImage] [nvarchar](max) NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  StoredProcedure [dbo].[pr_event_get_by_user]    Script Date: 9/4/2020 5:12:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[pr_event_get_by_user]
(
  @userId int
)
AS  
 BEGIN 
	SET NOCOUNT ON     
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED 
	SELECT *
	FROM   [Event] e
	WHERE UserId=@userId AND ISNULL(e.IsDeleted,0)=0
	ORDER BY  e.[Date] ASC 
END
GO
/****** Object:  StoredProcedure [dbo].[pr_event_get_list]    Script Date: 9/4/2020 5:12:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[pr_event_get_list]
(
 @tt_id tt_int READONLY 
)
AS
BEGIN
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED		
	
	CREATE TABLE #events(eventid INT)	
	
	INSERT INTO #events(eventid)
	SELECT e.id 	
	FROM @tt_id e

	  SELECT   e.EventId
              ,e.Title
              ,e.[Description]
              ,e.[Date]
              ,e.FromTime
              ,e.ToTime
			  ,e.[Location]
			  ,e.Notify
			  ,e.[Label]
			  , e.UserId
    FROM [Event] e
	JOIN #events b ON e.EventId = b.eventid	
	WHERE ISNULL(e.IsDeleted,0)=0
	ORDER BY e.EventId ASC
	
END
GO
/****** Object:  StoredProcedure [dbo].[pr_event_set]    Script Date: 9/4/2020 5:12:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[pr_event_set]
( 
	@eventId  int,
	@title  nvarchar(250) = null,
	@description  nvarchar(max) = null,
	@date  date = null,
	@fromTime  time(7) = null,
	@toTime  time(7) = null,
	@location  nvarchar(150) = null,
	@notify  nvarchar(150) = null,
	@label  nvarchar(50) = null,
	@userId  int
) 
AS   
 BEGIN 
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
	INSERT INTO  [Event]  VALUES ( 
			  @title
			, @description
			, @date
			, @fromTime
			, @toTime
			, @location
			, @notify
			, @label
			, 0
			, @userId
		)  
		SET  @eventId = SCOPE_IDENTITY(); 
		SELECT @eventId  EventId 
END
GO
/****** Object:  StoredProcedure [dbo].[pr_event_update]    Script Date: 9/4/2020 5:12:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[pr_event_update]  
(  
    @eventId  int,
	@title  nvarchar(250) = null,
	@description  nvarchar(max) = null,
	@date  date = null,
	@fromTime  time(7) = null,
	@toTime  time(7) = null,
	@location  nvarchar(150) = null,
	@notify  nvarchar(150) = null,
	@label  nvarchar(50) = null,
	@isDeleted bit
)  
 AS 
 BEGIN
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED

    UPDATE e WITH(ROWLOCK) SET 
	       e.Title=@title
         , e.[Description]=@description
		 , e.[Date]=@date
		 , e.FromTime=@fromTime
		 , e.ToTime=@toTime
		 , e.[Location]=@location
		 , e.Notify=@notify
		 , e.[Label]=@label
		 , e.IsDeleted=@isDeleted
	FROM [Event] e  WITH(ROWLOCK) 
	WHERE e.EventId = @eventId
End
GO
/****** Object:  StoredProcedure [dbo].[pr_events_get]    Script Date: 9/4/2020 5:12:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[pr_events_get]
AS  
 BEGIN 
	SET NOCOUNT ON     
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED 
	SELECT *
	FROM   [Event] e
	WHERE ISNULL(e.IsDeleted,0)=0
	ORDER BY  e.[Date] ASC 
END
GO
/****** Object:  StoredProcedure [dbo].[pr_user_get_by_email]    Script Date: 9/4/2020 5:12:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[pr_user_get_by_email] 
(
	@email nvarchar(250) = null
)
	AS  
	BEGIN SET NOCOUNT ON                      
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED    
	SELECT * FROM [User] u  WHERE u.Email = @email
	END
GO
/****** Object:  StoredProcedure [dbo].[pr_user_get_by_email_password]    Script Date: 9/4/2020 5:12:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[pr_user_get_by_email_password] 
(
	@email nvarchar(250) = null,
	@password nvarchar(Max) = null
)
	AS  
	BEGIN SET NOCOUNT ON                      
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED    
	SELECT * FROM [User] u  WHERE u.Email = @email and u.[Password] = @password
	END
GO
/****** Object:  StoredProcedure [dbo].[pr_user_set]    Script Date: 9/4/2020 5:12:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[pr_user_set]
( 
	@userId  int,
	@email  nvarchar(250) = null,
	@password  nvarchar(150) = null,
	@fullName  nvarchar(250) = null,
	@birthday  date = null,
	@profileImage  nvarchar(max) = null
) 
AS   
 BEGIN 
	SET NOCOUNT ON
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED  
	INSERT INTO  [User]  VALUES ( 
			  @email
			, @password
			, @fullName
			, @birthday
			, @profileImage
		)  
		SET  @userId = SCOPE_IDENTITY(); 
		SELECT @userId  userId 
END
GO
/****** Object:  StoredProcedure [dbo].[pr_users_get]    Script Date: 9/4/2020 5:12:30 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[pr_users_get]
AS  
 BEGIN 
	SET NOCOUNT ON     
	SET TRANSACTION ISOLATION LEVEL READ UNCOMMITTED 
	SELECT *
	FROM   [User] u
	ORDER BY  u.UserId DESC 
END
GO
